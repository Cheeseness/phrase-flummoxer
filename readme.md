# Phrase Flummoxer

A tool for running a phrase guessing game, based on a game that Lori and Corey Cole were using for giveaways during streams celebrating the 30th anniversary of Quest For Glory 1 (also known Hero's Quest).

1. Instructions
2. Licence



# 1. Instructions

## Installation

Clone the repository (or download via [this URL](https://gitlab.com/Cheeseness/phrase-flummoxer/-/archive/master/phrase-flummoxer-master.zip) and open index.html in a browser.


## General Instructions

1. Configure phrases by selecting "Set Up Phrases" from the main menu and adding/editing phrases as desired.
2. Configure any game settings as desired by selecting "Settings" from the main mneu. Individual settings are described below.
3. Select "Start Game" to begin the game with the first phrase.
4. Type letters on the keyboard as players use up their guesses. Letters within the phrase will automatically be revealed.
5. The round is won when the "Reveal" button is clicked, or when the last letter is guessed.
6. Repeat until all phrases have been played.


## Setting Up Phrases

1. Select "Set Up Phrases" from the main menu to open.
2. Click the "Add" button to add a new phrase, or a phrase's "Edit" button to edit that phrase.
3. Enter the phrase for players to guess letters for into the "Phrase" text area.
4. Optionally enter any additional notes to be displayed once the phrase is guessed/revealed into the "Note" text area.
5. Optionally enter a hexadecimal colour code prefixed with a # character into the "Colour override" text are if this phrase should use a different colour from the default colour or any background-specific colour.
6. Optionally select a background that the phrase should use if "Skip Background Overrides" is not set to true.


## Settings

### Greedy Letters
When "Greedy Letters" is set to true, a single guess will reveal all occurrences of that letter.

### Background Colour
Clicking apply will set the background colour to the hex colour value specified in the corresponding text field. Invalid values will not be applied.

### Skip Background Overrides
When "Skip Background Overrides" is set to true, per-phrase background overrides will be ignored.

### Skip Colour Overrides
When "Skip Colour Overrides" is set to true, per-phrase colour overrides will be ignored.

### Active Backgrounds
Clicking this will display a list of available backgrounds that can be toggled. If no backgrounds are active, no backgrounds will only be displayed for phrases that have background overrides configured *if* "Skip Background Overrides" is not set to true.



# 2. Licence

Phrase Flummoxer is made available under the Creative Commons CC0 1.0 Public Domain dedication
https://creativecommons.org/publicdomain/zero/1.0/

Third party dependencies may be distributed under alternative licences. Please check the third-party folder for more details.
